# Wiki Hackmeeting 2024

Este es el repositorio de la wiki del Hackmeeting 2024. Está creada con Hugo y el tema Blowfish, es la primera vez que probamos estas tecnologías para la wiki, ¡no dudéis en dar vuestras opiniones!

## Contribuir 

Para contribuir en el proyecto no es necesario tener Hugo instalado, ni ningún otro requisito especial. Simplemente modificando los archivos markdown que se encuentran en la carpeta `content` y haciendo un pull request es suficiente. Se puede editar desde el IDE Web integrado en Oxacab para abrir el entorno de desarrollo o descargando y ejecutando el proyecto en local. Podremos observar la estructura de directorios de un proyecto creado con Hugo. Si se quiere saber más sobre esta estructura, la mejor opción es ir directamente a la [documentación de Hugo sobre la estructura de directorios](https://gohugo.io/getting-started/directory-structure/).

Para editar la web, se requiere conocimientos básicos del [lenguaje de marcado Markdown](https://es.wikipedia.org/wiki/Markdown)[^1] y de su sintaxis[^2], además de una en cuenta en [0xacab](https://0xacab.org) (instancia de GitLab donde se aloja la página web).

[^1]: [Wikipedia, Markdown](https://es.wikipedia.org/wiki/Markdown)
[^2]: [Sintaxis Markdown](https://markdown.es/sintaxis-markdown/)

### Run

Para correr la wiki en local, es necesario tener instalado Hugo, [sigue estas instrucciones](https://gohugo.io/installation/linux/), para debian:

```bash
sudo apt install hugo
```

Para **instalar el tema Blowfish** hay **dos opciones**: i) instalándolo como módulo de Hugo o ii) como submódulo de git.

i) Actualizar el tema Blowfish ya instalado o añadir el tema como módulo de hugo:

```bash
# Actualizar el tema de hugo
hugo mod get -u
# [OPCIONAL] O añadir el tema (si no está instalado o se quiere añadir otro módulo)
hugo mod get github.com/nunocoracao/blowfish/v2
```

ii) O descargando el submódulo de git:

```bash
git submodule update --init --recursive --progress
```

El tema del Hackmeeting usa una template `custom.html` para la landing page ubicada en `partials/home/custom.html`. 
Para usar los layouts por defecto de blowfish puedes cambiar la propiedad layout en el `config/_default/_params.yaml`.

Para correr el server en local simplemente ejecuta el comando:

```bash
hugo server -p 1312
``` 

Y accede a `http://localhost:1312/`.

### Build

Para generar el sitio estático, simplemente ejecuta el comando 

```bash
hugo --baseURL="https://es.hackmeeting.org/latest/hugo"
``` 

Y se generará la carpeta `public` con el contenido