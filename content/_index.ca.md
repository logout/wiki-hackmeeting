---
title: "Hackstañazo HM2024"
description: "Hackstañazo HM2024"
---

<a href="docs/info/">
    <h1>  
        {{< icon "bell" >}} 26~29 de septiembre<br>
        {{< icon "location-dot" >}}Gaztaño Auzoa
    </h1>
</a>
Hackmeeting és la trobada anual de les contracultures digitals de la peninsula Iberica, d'aquelles comunitats que
analitzen de manera crítica els mecanismes de desenvolupament de les tecnologies en la nostra societat. Però hackmeeting no és
només això, és molt més. T'ho expliquem a cau d'orella, no li ho diguis a ningú, el hackmeeting és solament per a veritables
hackers, per als qui vulguin gestionar-se la vida com vulguin i lluiten per això, encara que no hagin vist un ordinador en la seva vida.

Tres dies de xerrades, jocs, festes, debats, intercanvis d'idees i aprenentatge col·lectiu, per a analitzar juntxs les
tecnologies que usem tots els dies, com canvien i com poden impactar en les nostres vides, tant reals com virtuals.
Una trobada per a indagar quin paper podem jugar en aquest canvi i alliberar-nos del control d'aquells que volen
monopolitzar el seu desenvolupament, trencant les nostres estructures socials i relegant-nos a espais virtuals cada vegada més limitats.

{{< alert " ">}}
**L'esdeveniment és totalment autogestionat: no hi ha ni organizador@s ni assistents, solament participants!**
{{< /alert >}}
