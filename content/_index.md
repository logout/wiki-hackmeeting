---
title: "Hackstañazo HM2024"
description: "Hackstañazo HM2024"
---

<a href="docs/info/">
    <h1>  
        {{< icon "bell" >}} 26~29 de septiembre<br>
        {{< icon "location-dot" >}}Gaztaño Auzoa
    </h1>
</a>

Hackmeeting es el encuentro anual de las contraculturas digitales de la peninsula Iberica, de aquellas comunidades que 
analizan de manera crítica los mecanismos de desarollo de las tecnologías en nuestra sociedad. Pero hackmeeting no es 
sólo esto, es mucho más. Te lo contamos al oído, no se lo digas a nadie, el hackmeeting es solamente para verdaderos 
hackers, para quienes quieran gestionarse la vida como quieran y luchan por eso, aunque no hayan visto un ordenador en su vida.

Tres días de charlas, juegos, fiestas, debates, intercambios de ideas y aprendizaje colectivo, para analizar juntxs las 
tecnologías que usamos todos los días, cómo cambian y cómo pueden impactar en nuestras vidas, tanto reales como virtuales. 
Un encuentro para indagar qué papel podemos jugar en este cambio y liberarnos del control de aquellos que quieren 
monopolizar su desarrollo, rompiendo nuestras estructuras sociales y relegándonos a espacios virtuales cada vez más limitados. 

{{< alert " ">}}
**El evento es totalmente autogestionado: no hay ni organizador@s ni asistentes, solamente participantes!**
{{< /alert >}}