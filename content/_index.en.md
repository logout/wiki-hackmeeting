---
title: "Hackstañazo HM2024"
description: "Hackstañazo HM2024"
---

<a href="docs/info/">
    <h1>  
        {{< icon "bell" >}} 26~29 of September<br>
        {{< icon "location-dot" >}}Gaztaño Auzoa
    </h1>
</a>

Hackmeeting is the annual meeting of the digital countercultures of the Iberian Peninsula, of those communities that critically analyze the mechanisms of development of technologies in our society. But hackmeeting it's not just that, it's much more. We'll tell you in your ear, don't tell anyone else, hackmeeting is just for real hackers, for those who want to manage life as they want and fight for it, even if they haven't seen a computer in their life.

Three days of talks, games, parties, open discussions, exchanges of ideas and collective learning, to analyze together the technologies we use every day, how they change and how they can impact our lives, both real and virtual.
A meeting to investigate what role we can play in this change and free ourselves from the control of those who want to monopolize their development, breaking our social structures and relegating us to increasingly limited virtual spaces.

{{< alert " ">}}
**The event is totally self-managed: there are neither organizers nor attendees, only participants!**
{{< /alert >}}