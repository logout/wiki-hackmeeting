---
title: "Información"
tags: ["Hackstañazo", "Como llegar", "Infrastructura"]
weight: 2
---

<div class="flex px-4 py-2 mb-8 text-base rounded-md bg-primary-100 dark:bg-primary-900">
  <span class="flex items-center ltr:pr-3 rtl:pl-3 text-primary-400">
    {{< icon "bell" >}}
  </span>
  <span class="flex items-center justify-between grow dark:text-neutral-300">
    <span class="prose dark:prose-invert">Del 26 al 29 de septiembre 2024</span>
  </span>
</div>
<div class="flex px-4 py-2 mb-8 text-base rounded-md bg-primary-100 dark:bg-primary-900">
  <span class="flex items-center ltr:pr-3 rtl:pl-3 text-primary-400">
    {{< icon "location-dot" >}}
  </span>
  <span class="flex items-center justify-between grow dark:text-neutral-300">
    <span class="prose dark:prose-invert">Gaztaño Auzoa</span>
  </span>
</div>

## Hackstañazo HM2024

Desde una prespectiva de barrio obrero, autogestionado y combativo, el Hackstañazo HM2024 es un espacio libre de 
agresiones, empresas y policías.

Se celebrará en Gaztaño Auzoa. Barrio obrero autogestionado a 7km de Donosti (Gipuzkoa).

![Cómo moverse](carteles/mapa_nodos.png "Nodos")

- [Cómo moverse](carteles/mapa_gaztetxe.svg)


## ¿Dónde dormir?

Se dormirá en el local de la antigua sindical del pueblo que tiene hueco para unas 40 personas. Si necesitamos más sitio podríamos usar la sala de conciertos Goiko, lo que nos daría 150 plazas más. Pero habría que pillar el tren y coger dos paradas. El tren funciona hasta las 00:00 cada 15 minutos, y el finde cada hora.

En caso de quedarse a dormir traer esterilla, almohada y manta.

Para dormir en furgonetas habrá un parking bastante grande cerca del espacio.

![Mapa Dormir](carteles/mapa_dormir.png "Dormir")

- [mapa_dormir.svg](carteles/mapa_lo_egin.svg)

## ¿Cómo llegar?

#### En coche

- [Instrucciones para llegar por autopista](autopista.pdf)
- [Instrucciones para llegar sin autopista](sinautopista.pdf)

Ubicación Gaztañoko Auzo-Lokala

Si tienes pensado ir en coche y te sobran plazas o si tienes pensado ir pero buscas trasporte entra en la página de inscripción:

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Formulario de Inscripción
{{< /button >}}

Aquí un pequeño mapas de zonas donde aparcar:

<iframe width="100%" height="300px" frameborder="0" allowfullscreen allow="geolocation" src="//umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true"></iframe><p><a href="//umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true">See full screen</a></p>

https://umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542

#### En Tren

Euskotren E2 (topo) Donostia - Irun

Parada: Errenteria

- [Instrucciones para llegar en tren](tren.pdf)

#### En Bus

Hay varios buses urbanos de diferentes sitios como el U7 y U33

Mas informacion en: http://lurraldebus.eus/es/linea-ordutegiak

## ¿Dónde comer?

La sociedad gastronómica del barrio (autogestionado) se encargará de dar los desayunos/comida/cena.

## Peques

Habra un espacio en el frontón dedicado a les peques.

## Animales de compañía

Se admiten mascotas mientras que no sean policías. En la sociedad gastronómica es el único sitio donde no podrán entrar. Pero tienen terraza. Y el barrio no es un sitio donde pasen coches, es tranquilo. 

## ¿Cómo nos autofinanciamos?

El Hackmeeting es un evento gratuito para quienes asisten. Los gastos que tenemos son la compra de camisetas para la serigrafía, herramientas o materiales para la adecuación del espacio y gastos de difusión. La autofinanciaciñón del evento se realiza aportando donaciones durante el encuentro y comprando las camisetas/sudaderas que serigrafiamos todos los años. Si estás interesade en mantener económicamente el evento, recuerda comprar tu camiseta :^)

## Logo

{{< figure
    src="https://humo.sindominio.net/apps/files_sharing/publicpreview/2oEn5wj3o8Scn5J?file=/&fileId=406003&x=1919&y=1080&a=true&etag=c2074dcaa53aeaba775ec1551075d6fb"
    alt="Hackstañazo HM2024"
    caption="Logo Hackstañazo HM2024"
    >}}

Podéis encontrar la propuesta de logo para [descargar aquí](https://humo.sindominio.net/s/2oEn5wj3o8Scn5J).

## Camisetas

Como forma de auto financiación, y como cada año, vamos a hacer camisetas con el logo propuesto de esta edición del hackmeeting.

Indica en el formulario de inscripción la talla de la camiseta para poder hecer un pedido previo.

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Formulario de Inscripción
{{< /button >}}

## PreHM

Unos días antes del Hackmeeting se harán trabajos de montaje, si quieres participar pregunta en la lista de correo.

## Infraestructura

- **Espacio vecinal**: Será el espacio central, donde pondremos una barra de bar y el infopoint. Esta gestionado por la gente del barrio. Tiene dos salas grandes: la de arriba es un espacio abierto con mesa y material para cacharrear, tiene terraza y se puede fumar. La sala de abajo tendrá el proyector y será donde daremos las charlas. 

- **Gaztexe del barrio**: 2 mins andando, espacio para conciertos, charlas, barra de bar, baños. Para charlas hay 2 salas. 

- **Radio**: Una sala, barra de bar, baños. 

- **Sociedad gastronómica**: papeo, cocina, y una sala para hacer charlas o comedor. 

- **Local de la antigua sindical del pueblo**: Sitio principal para dormir para unas 40 personas. 

- **Sala de conciertos Goiko**: a mayores 150 personas para dormir. Pero habría que pillar el tren y coger dos paradas. El tren funciona hasta las 00:00 cada 15 minutos, y el finde cada hora. 

- **Parking**: Hay una campa del barrio, se pedirá permiso. Hay sitio para furgos e incluso se podría acampar. 

- **Espacios abiertos**: Parque, frontón.

- **Conexión a internet**: Hay despliegue de fibra en el barrio, pero haremos un planteamiento propio para poner wifi e IPs públicas.

- **Fiesta**: Gaztexe, frontón y goiko eskola, pero la que mejor pinta tiene es el gaztexe. Tiene tanto interior como exterior.
