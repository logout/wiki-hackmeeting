---
title: "Informació"
tags: ["Hackstañazo", "Como arrivar", "Infrastructura"]
weight: 2
---

<div class="flex px-4 py-2 mb-8 text-base rounded-md bg-primary-100 dark:bg-primary-900">
  <span class="flex items-center ltr:pr-3 rtl:pl-3 text-primary-400">
    {{< icon "bell" >}}
  </span>
  <span class="flex items-center justify-between grow dark:text-neutral-300">
    <span class="prose dark:prose-invert">Del 26 al 29 de septiembre 2024</span>
  </span>
</div>
<div class="flex px-4 py-2 mb-8 text-base rounded-md bg-primary-100 dark:bg-primary-900">
  <span class="flex items-center ltr:pr-3 rtl:pl-3 text-primary-400">
    {{< icon "location-dot" >}}
  </span>
  <span class="flex items-center justify-between grow dark:text-neutral-300">
    <span class="prose dark:prose-invert">Gaztaño Auzoa</span>
  </span>
</div>

## Hackstañazo HM2024

Des d'una perspectiva de barri obrer, autogestionat i combatiu, el Hackstañazo HM2024 és un espai lliure d'agressions,
empreses i policies.

Es celebrarà a Gaztaño Auzoa. Barri obrer autogestionat a 7km de Donosti (Guipúscoa).

![Como moure's](carteles/mapa_nodos.png "Nodos")

- [Como moure's](carteles/mapa_gaztetxe.svg)


## ¿On Dormir?

S'adormirà en el local de l'antiga sindical del poble que té buit per a unes 40 persones. Si necessitem més lloc podríem usar la sala de concerts Goiko, la qual cosa ens donaria 150 places més. Però caldria enxampar el tren i agafar dues parades. El tren funciona fins a les 00.00 cada 15 minuts, i el cap de setmana cada hora.

En cas de quedar-se a dormir portar estoreta, coixí i manta.

Per a dormir en furgonetes hi haurà un pàrquing bastant gran prop de l'espai.

![Mapa Dormir](carteles/mapa_dormir.png "Dormir")

- [mapa_dormir.svg](carteles/mapa_lo_egin.svg)

## ¿Cóm arrivar?

#### Amb cotxe

- [Instruccions per arribar per autopista](autopista.pdf)
- [Instruccions per arribar sense autopista](sinautopista.pdf)

Ubicació Gaztañoko Auzo-Lokala

Si tens pensat anar amb cotxe i et sobren places o si tens pensat anar però busques transport entra en la pàgina d'inscripció:

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Formulari de Inscripció
{{< /button >}}

Aquí un petit mapa de zones on aparcar:

<iframe width="100%" height="300px" frameborder="0" allowfullscreen allow="geolocation" src="//umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true"></iframe><p><a href="//umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true">See full screen</a></p>

https://umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542

#### En Tren

Euskotren E2 (topo) Donostia - Irun

Parada: Errenteria

- [Instruccions per arribar en tren](tren.pdf)

#### En Bus

Hi ha diversos busos urbans de diferents llocs com l'U7 i U33

Més informació a: http://lurraldebus.eus/es/linea-ordutegiak

## ¿On menjar?

La societat gastronòmica del barri (autogestionat) s'encarregarà de donar els esmorçars/menjars/sopars.

## Peques

Hi haurà un espai en el frontó dedicat a lxs peques.

## Animals de companyia

S'admeten mascotes mentre que no siguin policies. En la societat gastronòmica és l'únic lloc on no podran entrar. Però tenen terrassa. I el barri no és un lloc on passin cotxes, és tranquil.

## ¿Com ens autofinancem?

El Hackmeeting és un esdeveniment gratuït per als qui assisteixen. Les despeses que tenim són la compra de samarretes per a la serigrafia, eines o materials per a l'adequació de l'espai i despeses de difusió. L'autofinançament de l'esdeveniment es realitza aportant donacions durant la trobada i comprant les samarretes/dessuadores que serigrafiem tots els anys. Si estàs interesade a mantenir econòmicament l'esdeveniment, recorda comprar la teva samarreta :^)

## Logo

{{< figure
    src="https://humo.sindominio.net/apps/files_sharing/publicpreview/2oEn5wj3o8Scn5J?file=/&fileId=406003&x=1919&y=1080&a=true&etag=c2074dcaa53aeaba775ec1551075d6fb"
    alt="Hackstañazo HM2024"
    caption="Logo Hackstañazo HM2024"
    >}}

Podeu trobar la proposta de logo per a [descarregar aquí](https://humo.sindominio.net/s/2oEn5wj3o8Scn5J).

## Samarretes


Com a forma d'acte finançament, i com cada any, farem samarretes amb el logo proposat d'aquesta edició del hackmeeting.

Indica en el formulari de incripció la talla de la samarreta per a poder fer una comanda prèvia.

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Formulario de Inscripción
{{< /button >}}

## PreHM

Uns dies abans del Hackmeeting es faran treballs de muntatge, si vols participar pregunta en la llista de correu.

## Infraestructura

- **Espai veinal**: Serà l'espai central, on posarem una barra de bar i el infopoint. Aquesta gestionat per la gent del barri. Té dues sales grans La de dalt és un espai obert amb taula i material per a cacharrejar, té terrassa i es pot fumar. La sala de baix tindrà el projector i serà on donarem les xerrades.

- **Gaztexe del barri**: 2 mins caminant, espai per a concerts, xerrades, barra de bar, banys. Per a xerrades n'hi han 2 sales.

- **Ràdio**: Una sala, barra de bar, banys. 

- **Societat gastronòmica**: papeo, cuina, i una sala per a fer xerrades o menjador.

- **Local de l'antiga sindical del poble**: Lloc principal per a dormir per a unes 40 persones.

- **Sala de concerts Goiko**: Com a molt 150 persones per a dormir. Però caldria agafar el tren i fer dues parades. El tren funciona fins a les 00.00 cada 15 minuts, i el cap de setmana cada hora.

- **Parking**: Hi ha un camp del barri, es demanarà permís. Hi ha lloc per a furgos i fins i tot es podria acampar.

- **Espais a l'aire lliure**: Parc, frontó.

- **Conexió a internet**: Hi ha desplegament de fibra en el barri, però farem un plantejament propi per a posar wifi i IPs públiques.

- **Festa**: Gaztexe, frontó y goiko eskola, però la que millor pinta té és el gaztexe. Té tant interior com exterior.

