---
title: "Information"
tags: ["Hackstañazo", "How to get there", "Infrastructure"]
weight: 2
---

<div class="flex px-4 py-2 mb-8 text-base rounded-md bg-primary-100 dark:bg-primary-900">
  <span class="flex items-center ltr:pr-3 rtl:pl-3 text-primary-400">
    {{< icon "bell" >}}
  </span>
  <span class="flex items-center justify-between grow dark:text-neutral-300">
    <span class="prose dark:prose-invert">From 26 to 29 September 2024</span>
  </span>
</div>
<div class="flex px-4 py-2 mb-8 text-base rounded-md bg-primary-100 dark:bg-primary-900">
  <span class="flex items-center ltr:pr-3 rtl:pl-3 text-primary-400">
    {{< icon "location-dot" >}}
  </span>
  <span class="flex items-center justify-between grow dark:text-neutral-300">
    <span class="prose dark:prose-invert">Gaztaño Auzoa</span>
  </span>
</div>

## Hackstañazo HM2024

From a prespective of the working class, self-managed and combative neighborhood, the Hackstañazo HM2024 is a space free of aggression, companies and police.

It will be held in Gaztaño Auzoa. Self-managed working quarter 7km from Donosti (Gipuzkoa).

![How to get around](/docs/info/carteles/mapa_nodos.png "Nodes")

- [How to get around](/docs/info/carteles/mapa_gaztetxe.svg)

## Where to sleep?

You will sleep at the site of the former trade union of the village that has room for about 40 people. If we need more room we could use the Goiko concert hall, which would give us 150 more places. But we'd have to catch the train and get two stops. The train runs until 00:00 every 15 minutes, and finds every hour.

In case of staying to sleep bring mat, pillow and blanket.

To sleep in furgos there will be a fairly large parking near the space.

![Sleep Map](/docs/info/carteles/mapa_dormir.png "Sleep")

- [Sleep_map.svg](/docs/info/carteles/mapa_lo_egin.svg)

## How to get there?

#### By car

Location Gaztañoko Auzo-Lokala

If you are planning to go by car and you have places left over or if you are planning to go but are looking for transport, please go to the registration page:

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Registration form
{{< /button >}}

Here is a small map of parking areas:

<iframe width="100%" height="300px" frameborder="0" allowfullscreen allow="geolocation" src="//umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true"></iframe><p><a href="//umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542?scaleControl=false&miniMap=false&scrollWheelZoom=true&zoomControl=true&editMode=disabled&moreControl=true&searchControl=null&tilelayersControl=null&embedControl=null&datalayersControl=true&onLoadPanel=none&captionBar=false&captionMenus=true">See full screen</a></p>

https://umap.openstreetmap.fr/eu/map/hackmeeting-2024_1106542

#### By train

Euskotren E2 (topo) Donostia - Irun

Stop: Errenteria

#### By bus

There are several urban buses from different sites such as U7 and U33

More information in: http://lurraldebus.eus/es/linea-ordutegiak

## Where to eat?

The gastronomic society of the neighborhood (self-managed) will be responsible for giving breakfast/food/dinner.

## Little ones

There will be a space in the fronton dedicated to the little ones.

## Companion animals

Pets are allowed while they are not cops. In gastronomic society it is the only place where they cannot enter. But they have a terrace. And the neighborhood isn't a place where cars pass, it's quiet.

## How do we self-finance?

Hackmeeting is a free event for those who attend. The expenses we have are the purchase of silkscreen T-shirts, tools or materials for the adequacy of space and diffusion expenses. The self-financing of the event is made by providing donations during the meeting and buying the t-shirts/hoodies that we select every year. If you are interested in keeping the event financially, remember to buy your T-shirt :^)

## Logo

{{< figure
    src="https://humo.sindominio.net/apps/files_sharing/publicpreview/2oEn5wj3o8Scn5J?file=/&fileId=406003&x=1919&y=1080&a=true&etag=c2074dcaa53aeaba775ec1551075d6fb"
    alt="Hackstañazo HM2024"
    caption="Hackstañazo HM2024 Logo"
    >}}

You can find the logo proposal to [download here](https://humo.sindominio.net/s/2oEn5wj3o8Scn5J).

## T-Shirts

As a form of self-funding, and as every year, we will make t-shirts with the proposed logo of this edition of the hackmeeting.

Indicate in the registration form the size of the t-shirt in order to obtain a previous order.

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Registration form
{{< /button >}}

## PreHM

A few days before the Hackmeeting there will be assembly work, if you want to participate ask on the mailing list.

## Infrastructure

- **Neighborhood space**: It will be the central space, where we will put a bar and infopoint. It's managed by the neighborhood people. It has two large rooms. The one above is an open space with tables and material for tinker, has a terrace and can be smoked. The downstairs room will have the projector and will be where we will be the talks.

- **Gaztexe of the neighborhood**: 2 mins walking, concert space, talks, bar, bathrooms. For talks there are 2 rooms.

- **Radio**: A room, bar, bathrooms.

- **Gastronomic society**: food, kitchen, and a room to make talks or dining.

- **Space of the former trade union of the village**: Main place to sleep for about 40 people.

- **Goiko concert hall**: more than 150 people to sleep. But we'd have to catch the train and get two stops. The train runs until 00:00 every 15 minutes, and on weekends every hour. 

- **Parking**: There is a land without trees in the neighborhood. There is space for vans and you could even camp.

- **Open spaces**: Park, court wall.

- **Internet Connection**: There is fiber deployment in the neighborhood, but we will make our own approach to put wifi and public IPs.

- **Party**: Gaztexe, court wall and goiko eskola, but the best place is the gaztexe. It has both interior and exterior.