---
title: "Participa"
tags: ["Contacto", "Comunicación"]
weight: 5
---

## Participa

El Hackmeeting es un evento autogestionado y horizontal, lo que significa que todas las personas que participan en él son responsables de su organización. Por eso, ¡tú también puedes participar!

Disponemos de varios canales de comunicación  para que puedas colaborar en la organización del evento:

Sigue el proceso de construcción a través de:

- [Lista de correo](https://listas.sindominio.net/mailman/listinfo/hackmeeting): medio principal de comunicación. Este es el sitio para estar a la última de todo lo que se cuece.
- [Canal de Matrix](https://riot.im/app/#/room/#hackmeeting:matrix.org) (canal público. Solo lectura para anónimes): para comunicaciones más efímeras y rápidas.
- [Pad de actas](https://pad.riseup.net/p/Hackmeeting-2024-actas-keep): donde se toman las actas de las asambleas.
- [Sala de asambleas](https://meet.guifi.net/Hackmeeting-2024): sala de videoconferencias para las asambleas.