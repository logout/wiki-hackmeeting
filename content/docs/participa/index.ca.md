---
title: "Participa"
tags: ["Contacte", "Comunicació"]
weight: 5
---

## Participa


El Hackmeeting és un esdeveniment autogestionat i horitzontal, cosa que significa que totes les persones que participen en ell són responsables de la seva organització. Per això, tu també pots participar!

Disposem de diversos canals de comunicació perquè puguis col·laborar en l'organització de l'esdeveniment:

Segueix el procés de construcció a través de:

- [Llista de correu](https://listas.sindominio.net/mailman/listinfo/hackmeeting): mig principal de comunicació. Aquest és el lloc per a estar a l'última de tot el que es cou.
- [Canal de Matrix](https://riot.im/app/#/room/#hackmeeting:matrix.org) (canal públic. Només lectura per a anónimes): per a comunicacions més efímeres i ràpides.
- [Pad d'actes](https://pad.riseup.net/p/Hackmeeting-2024-actas-keep): on es prenen les actes de les assemblees.
- [Sala d'assamblees](https://meet.guifi.net/Hackmeeting-2024): sala de videoconferències per a les assemblees.
