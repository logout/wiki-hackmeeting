---
title: "Participate"
tags: ["Contact", "Communication"]
weight: 5
slug: participate
---

## Participate

Hackmeeting is a self-managed and horizontal event, which means that all people participating in it are responsible for their organization. That's why you can participate too!

We have several channels of communication so you can collaborate in organizing the event:

Follow the construction process through:

- [Mailing list](https://listas.sindominio.net/mailman/listinfo/hackmeeting): The main way of communication. This is the place to stay up to date with the latest news.
- [Matrix Channel](https://riot.im/app/#/room/#hackmeeting:matrix.org) (public channel. Read-only for anonymous people): for more ephemeral and quicker communications.
- [Minutes pad](https://pad.riseup.net/p/Hackmeeting-2024-actas-keep): where the minutes of the assemblies are taken.
- [Assembly room](https://meet.guifi.net/Hackmeeting-2024): videoconference room for assemblies.