---
title: "Registration"
tags: ["Registration", "Car sharing", "Accessibility", "Foods"]
weight: 3
slug: registration
---

It is important to register to know how many people we are to sleep and to eat. To get an idea of how many people are going to sleep, size of t-shirts, how many people we are going to eat, etc., we have enabled a registration form.

**Register through the form on this page**

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Registration form
{{< /button >}}

If you have any need that we have not taken into account you can indicate it in the registration form, but note that the results are public.

If you want to get in touch through [our forms of contact](/docs/participa) and transmit your doubts or requests.