---
title: "Inscripció"
tags: ["Inscripció", "Compartir coche", "Accessibilitat", "Menjars"]
weight: 3
---

És important inscriure's per a poder saber quantes som per a dormir i per a menjar. Per a tenir una idea de quanta gent es quedarà a dormir, talles de samarretes, quanta gent tindrem per a menjar, etc, hem habilitat un formulari d'inscripció.

**Inscriure's a través del formulari que hi ha en aquesta pàgina**

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Formulari d'inscripció
{{< /button >}}

Si tens alguna necessitat que no hem tingut en compte pots indicar-ho en el formulari d'inscripció, però tingues en compte que els resultats són públics.

Si vols pots posar-te en contacte a través de [les nostres formes de contacte](/docs/participa) i transmetre els teus dubtes o peticions.
