---
title: "Inscripción"
tags: ["Inscripción", "Compartir coche", "Accesibilidad", "Comidas"]
weight: 3
---

Es importante inscribirse para poder saber cuantas somos para dormir y para comer. Para tener una idea de cuanta gente se va a quedar a dormir, tallas de camisetas, cuanta gente vamos a tener para comer, etc, hemos habilitado un formulario de inscripción.

**Inscribirse a través del formulario que hay en esta pagina**

{{< button href="https://forms.komun.org/inscripcion-hackmeeting-2024" target="_self" >}}
Formulario de inscripción
{{< /button >}}

Si tienes alguna necesidad que no hemos tenido en cuenta puedes indicarlo en el formulario de inscripción, pero ten en cuenta que los resultados son públicos.

Si quieres puedes ponerte en contacto a través de [nuestras formas de contacto](/docs/participa) y transmitir tus dudas o peticiones.
