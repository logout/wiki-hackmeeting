---
title: "Nota de prensa"
tags: ["Nota de prensa"]
weight: 1
slug: prensa
---
## Euskera

Datorren irailaren 26tik 29ra egingo da aurtengo hackmeetinga Gaztañon (Orereta), Gipuzkoan. Hackmeeting-a urtero egiten den topaketa autogestionatua da, 2000. urteaz geroztik egiten dena, Espainiako estatu osoan zehar intineratuz. Bertako bizilagunak, artistak, aktibista sozialak eta aktibista informatzaileak (hacktibistak), Estatu osoko benideak eta bizilagunak biltzen ditu, batez ere Italiako lurraldeetakoak.

Lau egunez elkarrekin biziko dira, ezagutzak partekatuz, eztabaidatuz, jolastuz, kakotuz eta sarea eginez, bai Interneten, bai gizartean. 4 egunetarako parrillak 4 aretotan antolatzen ditu "nodo" guztiak, hau da, bere buruari deitutako edozein jarduera: hitzaldiak, tailerrak, eztabaidak, jolasak... eta gauez, umore pixka bat hitzaldi azkarrekin eta karaokearekin, eta festa pixka bat kontzertu eta musika zulatuarekin. Egun bakoitzean kudeaketa batzar bat egongo da eta igandekoa ekitaldia baloratuko da eta hurrengo urteari begira ardurak txandakatuko dira.

Une honetan segurtasun digital transfeministari, telefonoetako segurtasunari, zaintza komertzial eta politikoari, teknologia dekrecentista eta solarpunkari, agenda komunitarioei, ekitaldien sonorizazioari, sare sozialei, adimen artifizialari, zuntz optikoko sareei, aniztasunari eta inklusibitateari buruzko nodoak daude, hacktivismoaren egungo erronkei buruzko eztabaidak, eta abar.

Aurten generoko espazio ez-misto bat erreserbatu da, non gizon giza-sexualak ez diren pertsona guztiak ongi etorriak dauden. Hiru mistoekin paraleloak diren nodo propioak dituen espazioa izango da.

Irailaren 18an behin-behineko parrilla bat argitaratu zen, aldaketak eta nodo gehiago dituena. Hemen dago: http://lanbroa.maritxusarea.eus/s/74Z3TAxNaHJrtzF/download/parrillafinala.png
Aldaketa garrantzitsuak helbide honetan iragarriko dira: https://xarxa.cloud/@hackmeeting /

Aurtengo hausnarketa eta helburu politikoa biltzen dituen manifestua hemen dago: https://es.hackmeeting.org/latest/hugo/docs/sobre/#manifiesto-2024

Informazio gehiago nahi izanez gero, kontsultatu https://es.hackmeeting.org/latest/hugo/eta fedibertsoaren kontua https://xarxa.cloud/@hackmeeting /

Prentsa-kontaktuetarako, helbidera hackmeeting@sindominio.net


## Català

Els pròxims dies del 26 al 29 de setembre se celebrarà el Hackmeeting d'enguany en Gaztaño (Orereta), Guipúscoa. El 
Hackmeeting és una trobada anual autogestionada que es porta celebrant des de l'any 2000 itinerant pel tot l'estat 
espanyol. Reuneix veïnes del lloc, artistes, activistes socials i activistes informàtiques (hacktivistes), vingudes de 
tot el territori estatal i dels veïns, especialment de terres italianes.

Conviuran 4 dies compartint coneixements, debatent, jugant, trastejant i fent xarxa, tant d'internet com social. La 
graella per als 4 dies organitza en 4 sales tots els "nodes", que són qualsevol activitat autoconvocada: xerrades, 
tallers, debats, jocs, ... i a la nit, una mica d'humor amb les xerrades ràpides i el karaoke propi, i una mica de festa 
amb concert i música punxada. Cada dia hi haurà una assemblea de gestió i la del diumenge es valorarà l'esdeveniment i 
es giraran responsabilitats en mires de l'any següent.

En aquests moments ja hi ha nodes apuntats sobre seguretat digital transfeminista, seguretat en telèfons, vigilància 
comercial i política, tecnologies decrecentistas i solarpunk, agendes comunitàries, sonorització d'esdeveniments, xarxes 
socials, intel·ligència artificial, xarxes de fibra òptica, diversitat i inclusivitat, debats sobre els reptes actuals 
de l'hacktivisme, i un llarg etcètera.

Enguany s'ha reservat un espai no mixt de gènere, on estan benvingudes totes les persones que no siguin homes cis-sexuals. 
Serà un espai amb nodes propis paral·lels als tres mixtos.

Hi ha publicada una graella provisional a data de 18 de setembre, que està subjecta a canvis i a més nodes. Aquesta es troba en http://lanbroa.maritxusarea.eus/s/*74Z3TAxNaHJrtzF/*download/*parrillafinala.png
Els canvis rellevants s'anunciaran en https://xarxa.cloud/@hackmeeting/

El manifest amb la reflexió i propòsit polític d'enguany es troba en https://es.hackmeeting.org/latest/hugo/docs/sobre/#manifest-2024

Per a més informació, consulteu https://es.hackmeeting.org/latest/hugo/ i el compte del fediverso https://xarxa.cloud/@hackmeeting/

## Castellano

Los próximos días del 26 al 29 de septiembre se celebrará el Hackmeeting de este año en Gaztaño (Orereta), Gipuzkoa.
El Hackmeeting es un encuentro anual autogestionado que se lleva celebrando desde el año 2000 itinerando por el todo el
estado español. Reúne vecines del lugar, artistas, activistas sociales y activistas informátiques (hacktivistas), venides
de todo el territorio estatal y de los vecinos, especialmente de tierras italianas.

Van a convivir 4 días compartiendo conocimientos, debatiendo, jugando, cacharreando y haciendo red, tanto de internet
como social. La parrilla para los 4 días organiza en 4 salas todos los "nodos", que son cualquier actividad
autoconvocada: charlas, talleres, debates, juegos, ... y por la noche, un poco de humor con las charlas rápidas y el
karaoke propio, y un poco de fiesta con concierto y música pinchada. Cada día habrá una asamblea de gestión y la del
domingo se valorará el evento y se rotarán responsabilidades en miras del año siguiente.

En estos momentos ya hay nodos apuntados sobre seguridad digital transfeminista, seguridad en teléfonos, vigilancia
comercial y política, tecnologías decrecentistas y solarpunk, agendas comunitarias, sonorización de eventos, redes
sociales, inteligencia artificial, redes de fibra óptica, diversidad e inclusividad, debates sobre los retos actuales
del hacktivismo, y un largo etcétera.

Este año se ha reservado un espacio no mixto de género, donde están bienvenidas todas las personas que no sean hombres
cis-sexuales. Será un espacio con nodos propios paralelos a los tres  mixtos.

Hay publicada una parrilla provisional a fecha de 18 de septiembre, que está sujeta a cambios y a más nodos. Esta se
encuentra en http://lanbroa.maritxusarea.eus/s/74Z3TAxNaHJrtzF/download/parrillafinala.png

Los cambios relevantes se anunciarán en https://xarxa.cloud/@hackmeeting/

El manifiesto con la reflexión y propósito político de este año se encuentra en https://es.hackmeeting.org/latest/hugo/docs/sobre/#manifiesto-2024

Para más información, consultad https://es.hackmeeting.org/latest/hugo/ y la cuenta del fediverso https://xarxa.cloud/@hackmeeting/