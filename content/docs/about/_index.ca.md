---
title: "Sobre el Hackmeeting"
tags: ["Història", "Nota de premsa", "Manifest"]
weight: 1
slug: sobre
---

El Hackmeeting és un espai autogestionat de trobada on compartir inquietuds tecno-polítiques.

Des-de la primera trobada en 1998 fins avui, ha estat un espai perquè gent **amb o sense coneixements tècnics** pugui
trobar-se, plantejar dubtes, generar debats o compartir coneixements en un entorn on allò social i la tecnologia són
les protagonistes.

## Manifiest 2024

Any 2024: Fins on el record ens arriva, sabem que el "sistema", en les mútiples manifestacions del seu poder, es perpetua a si mateix a través del reemplaçament de les elits, sobrevivint, generació rere generació, a la mortalitat de l'ésser humà i a l'efímer de la seva naturalesa.

Distintes han estat les eines emprades per a mantenir l'estructura de poder, perfeccionades, modificades, dissenyades, inventades, adaptades per a sobreviure al llarg del temps i incrementar el domini sobre el món, i sobre els qui realment fan la història, encara que no figurin en ella.

Són subtils els seus mètodes, adaptats a cada època, i sempre assimilats com si fossin part de la "naturalesa humana", justificant així atroços exercicis de somentimiento de l'ésser humà i de la seva veritable essència. Disfressades de "Déus i Dimonis", del bé i del mal, de pecat i exemplaritat, d'amics i enemics... les elits mantenen activa la guerra que lliura l'ésser humà contra si mateix, confós, odiant-se a si mateix i estimant a les elits que li prometen la salvació mentre l'"humilien, assimilen".

No obstant això, per molt que es perfeccioni, el sistema de poder mai aconseguirà la seva finalitat, perquè precisa de la "VIDA", la continuïtat de les múltiples vides mortals que habiten el món, per a perpetuar-se, i aquesta no pot doblegar-se completament sense deixar d'existir. L'essència de la llibertat no es pot dominar. Igual que els cicles d'un cristall de quars ajusten el ritme del pensament digital, la mort ressona en la línia de temps i s'obren finestres d'esperança que vibren en les ments dels qui escriuen el món però no figuren en la histora, mantenint desperta al llarg del temps (i igual que com el "sistema" sobreviu), la "consciència" dels de "baix".

Prometeu va robar el foc als Déus per a benefici de tota la humanitat, però les elits ens han robat el sentit del temps per a sotmetre'ns i robar-nos tota esperança de canvi. El sentit del temps ens situa simultàniament en la linealitat de la història (social i personal) i els cicles vitals que la nodreixen. En robar-nos-ho creen un temps fals, amb els seus cicles de producció i consum, de treball i d'oci, que ens allunya de la naturalesa i de la possibilitat de donar-li sentit a les nostres vides. Amb la industralització i la domesticació de la mà d'obra, acostumada a l'anarquia del temps natural, als cicles solars i lunars, apareixen els "horaris" controlats per rellotges que serveixen a l'amo per a sotmetre a la recentment apareguda classe obrera. Així, el temps va perdre la seva naturalesa per a industrialitzar-se al servei de les elits.

Fruit de tot aquest procés, hem perdut el sentit de la "història" i amb això la possibilitat de refutar el nostre present. Amb l'avanç de la tecnologia, internet i la creació de les xarxes socials, "aquells que escrivim el món però no figurem en la història", som esclaus del temps, hem perdut la perspectiva del passat i la màxima que "el futur és ara" ja no té validesa. El futur és passat i això ens obliga a precipitar-nos en una vida que no entenem per a ser acceptats socialment. Ja no mirem ni analitzem el passat per a entendre el present i pensar el futur. Per això, repetim els mateixos esquemes que ens van portar al fracàs, sense temps per a reflexionar, dominats per infames horaris i manipulats per noves elits que disfressen els seus missatges de "alliberament" per a ampliar l'abast del seu domini.

A nosotrxs, lxs hackers, lxs activistes, que també escrivim el món, no ens importa ser socialment "acceptats" en el món de les elits. Lluitem contra el seu domini, escoltant la ressonància de la mort en la línia del temps per a continuar alimentant consciències, aunant forces per a enderrocar el sistema. Nosotrxs lxs hackers, lxs activistes, mirem al passat orgullosxs de mantenir viva l'herència dels qui ens van precedir en la lluita, aprenent de ellxs i de com van usar les eines de la seva època en contra del sistema d'opressió.

Per això, enguany us convoquem al HACKSTAÑAZO, el hackmeeting de 2024 que se celebrarà en Gaztaño, una illa on la paraula temps encara manté la seva naturalitat i la resistència és una herència conscient. Una terra on encara està viva la memòria de les bruixes, com Maritxu Mozkorra, que van resistir el desencantament forçat del món per les elits de la modernitat. En aquesta trobada us convidem a reencantar la tecnologia amb el pensament i la rebel·lia hacker, a compartir coneixements i experiències per a construir nous mitjans de lluita o adaptar els que hem heretat d'altres èpoques als nous escenaris de confrontació social.

Nosaltrxs, lxs hacker, lxs activistes, igual que Prometeu, volem continuar mantenint el foc de la lluita. Nosaltrxs Resistim!!! Visqui la Resistència!!

## Nota de premsa

**[Llegeix la nota de premsa aqui](/docs/press)**

{{< article link="/docs/press" >}}

## Història y pilars

{{< alert "edit">}}
**TODO!**
{{< /alert >}}
