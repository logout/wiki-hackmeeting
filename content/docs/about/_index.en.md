---
title: "About Hackmeeting"
tags: ["History", "Press release", "Manifesto"]
weight: 1
slug: about
---

Hackmeeting is a self-managed meeting space where to share techno-political concerns.

From the first meeting in 1998 to today, it has been a space for people **with or without technical knowledge** to find themselves, raise doubts, generate debate or share knowledge in an environment where social and technology are the protagonists.

## History and pillars

{{< alert "edit">}}
**TODO!**
{{< /alert >}}

## Press release

{{< alert "edit">}}
**TODO!**
{{< /alert >}}

## Manifesto 2024

Year 2024: As far as memory reaches us, we know that the "system", in the multiple manifestations of its power, perpetuates itself through the replacement of the elites, surviving, generation after generation, the mortality of the human being and the ephemeral of its nature.

Different tools have been used to maintain the power structure, perfected, modified, designed, invented, adapted to survive over time and to increase dominance over the world, and on those who really do the story, even if they do not appear in it.

They are subtle their methods, adapted to each time, and always assimilated as if they were part of the "human nature", thus justifying atrocious exercises of soment of the human being and its true essence. Disguised of "Gods and Demons", of good and evil, of sin and exemplary, of friends and enemies... the elites keep active the war waged by the human being against himself, confused, hating himself and loving the elites who promise him salvation while "humiliating, assimilating".

However, as much as it is perfected, the system of power will never achieve its purpose, because it requires the "Life", the continuity of the many mortal lives that inhabit the world, to perpetuate itself, and this cannot be completely doubled without failing to exist. The essence of freedom cannot be dominated. Like the cycles of a quartz crystal adjust the rhythm of digital thinking, death resonates in the timeline and windows of hope are opened that vibrate in the minds of those who write the world but do not appear in the storyteller, keeping awake over time (and as the "system" survives), the "consciousness" of those "below".

Prometheus stole the fire from the Gods for the benefit of all humanity, but the elites have robbed us of the sense of time to submit to us and steal all hope of change. The sense of time places us simultaneously in the linearity of history (social and personal) and the life cycles that nourish it. By stealing it, they create a false time, with their cycles of production and consumption, of work and leisure, that drives us away from nature and the possibility of giving meaning to our lives. With the industralization and domestication of the labor force, accustomed to the anarchy of natural time, to the solar and lunar cycles, appear the "schedules" controlled by watches that serve the master to submit to the newly-appointed working class. Thus, time lost its nature to industrialize itself at the service of the elites.

As a result of this process, we have lost the meaning of "history" and thus the possibility of refusing our present. With the advance of technology, the internet and the creation of social networks, "those who write the world but do not appear in history", we are slaves of time, we have lost sight of the past and the maxim that "the future is now" is no longer valid. The future is past and this forces us to rush into a life we do not understand to be socially accepted. We no longer look or analyze the past to understand the present and think the future. Therefore, we repeat the same schemes that led us to failure, without time to reflect, dominated by infamous hours and manipulated by new elites who disguise their "liberation" messages to expand the scope of their domain.

To us, hackers, activists, who also write the world, we do not care to be socially "accepted" in the elite world. We fight against his dominion, listening to the resonance of death on the timeline to continue feeding consciousness, joining forces to overthrow the system. We hackers, activists, look at the past prides of keeping alive the heritage of those who preceded us in the struggle, learning from them and how they used the tools of their time against the system of oppression.

That is why this year we call you to HACKSTAÑAZO, the hackmeeting of 2024 to be held in Gaztaño, an island where the word time still maintains its naturality and resistance is a conscious heritage. A land where the memory of the witches is still alive, such as Maritxu Mozkorra, which resisted the forced disinclusionment of the world by the elites of modernity. In this meeting we invite you to reenchant technology with hacker thought and rebellion, to share knowledge and experiences to build new means of struggle or to adapt those that we have inherited from other times to the new scenarios of social confrontation.

We, the hackers, the activists, like Prometheus, want to keep the fire of the struggle. We Resist!!! Long live the Resistance!!