---
title: "Press release"
tags: ["Press release"]
weight: 1
slug: press
---
The upcoming Hackmeeting will take place from September 26 to 29 in Gaztaño (Orereta), Gipuzkoa. The Hackmeeting is a self-organized annual gathering that has been held since 2000, traveling around different locations across Spain. It brings together local residents, artists, social activists, and IT activists (hacktivists) from all over Spain and neighboring countries, especially from Italy.

For four days, participants will live together, sharing knowledge, debating, playing, tinkering, and networking, both online and socially. The schedule for these four days is organized into four rooms where all the "nodes" will take place—these are any self-organized activities: talks, workshops, debates, games, and in the evenings, some humor with lightning talks, karaoke, and a bit of a party with concerts and DJ music. Each day, there will be a management assembly, and on Sunday, the event will be evaluated, and responsibilities will be rotated for the following year.

So far, nodes have been registered on topics such as transfeminist digital security, phone security, commercial and political surveillance, degrowth and solarpunk technologies, community agendas, event sound design, social networks, artificial intelligence, fiber optic networks, diversity and inclusivity, debates on the current challenges of hacktivism, and much more.

This year, a non-mixed gender space has been reserved, welcoming everyone except cisgender men. This space will feature its own nodes alongside the three mixed ones.

A provisional schedule has been published as of September 18, subject to changes and the addition of more nodes. It can be found at [this link](http://lanbroa.maritxusarea.eus/s/74Z3TAxNaHJrtzF/download/parrillafinala.png). Relevant changes will be announced at [this link](https://xarxa.cloud/@hackmeeting/).

The manifesto with this year's reflections and political purpose is available at [this link](https://es.hackmeeting.org/latest/hugo/docs/sobre/#manifiesto-2024).

For more information, check [this site](https://es.hackmeeting.org/latest/hugo/) and the fediverse account [here](https://xarxa.cloud/@hackmeeting/).