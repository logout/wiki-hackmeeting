---
title: "Sobre el Hackmeeting"
tags: ["Historia", "Nota de prensa", "Manifiesto"]
weight: 1
slug: sobre
---

El Hackmeeting es un espacio autogestionado de encuentro donde compartir inquietudes tecno-políticas.

Des del primer encuentro en 1998 hasta hoy, ha sido un espacio para que gente **con o sin conocimientos técnicos** pueda 
encontrarse, plantear dudas, generar debates o compartir conocimientos en un entorno donde lo social y la tecnología son 
las protagonistas.

## Manifiesto 2024

Año 2024: Hasta donde el recuerdo nos alcanza, sabemos que el "sistema", en las mútiples manifestaciones de su poder, se perpetúa a sí mismo a través del reemplazo de las élites, sobreviviendo, generación tras generación, a la mortalidad del ser humano y a lo efímero de su naturaleza.

Distintas han sido las herramientas empleadas para mantener la estructura de poder, perfeccionadas, modificadas, diseñadas, inventadas, adaptadas para sobrevivir a lo largo del tiempo e incrementar el dominio sobre el mundo, y sobre quienes realmente hacen la historia, aunque no figuren en ella.

Son sutiles sus métodos, adaptados a cada época, y siempre asimilados como si fueran parte de la "naturaleza humana", justificando así atroces ejercicios de somentimiento del ser humano y de su verdadera esencia. Disfrazadas de "Dioses y Demonios", del bien y del mal, de pecado y ejemplaridad, de amigos y enemigos... las élites mantienen activa la guerra que libra el ser humano contra sí mismo, confundido, odiándose a sí mismo y amando a las élites que le prometen la salvación mientras lo "humillan, asimilan".

Sin embargo, por mucho que se perfeccione, el sistema de poder nunca logrará su finalidad, porque precisa de la "VIDA", la continuidad de las múltiples vidas mortales que habitan el mundo, para perpetuarse, y esta no puede doblegarse completamente sin dejar de existir. La esencia de la libertad no se puede dominar. Al igual que los ciclos de un cristal de cuarzo ajustan el ritmo del pensamiento digital, la muerte resuena en la línea de tiempo y se abren ventanas de esperanza que vibran en las mentes de quienes escriben el mundo pero no figuran en la histora, manteniendo despierta a lo largo del tiempo (y al igual que como el "sistema" sobrevive), la "conciencia" de los de "abajo".

Prometeo robó el fuego a los Dioses para beneficio de toda la humanidad, pero las élites nos han robado el sentido del tiempo para someternos y robarnos toda esperanza de cambio. El sentido del tiempo nos sitúa simultáneamente en la linealidad de la historia (social y personal) y los ciclos vitales que la nutren. Al robárnoslo crean un tiempo falso, con sus ciclos de producción y consumo, de trabajo y de ocio, que nos aleja de la naturaleza y de la posibilidad de darle sentido a nuestras vidas. Con la industralización y la domesticación de la mano de obra, acostumbrada a la anarquía del tiempo natural, a los ciclos solares y lunares, aparecen los "horarios" controlados por  relojes que sirven al amo para someter a la recién aparecida clase obrera. Así, el tiempo perdió su naturaleza para industrializarse al servicio de las élites.

Fruto de todo este proceso, hemos perdido el sentido de la "historia" y con ello la posibilidad de refutar nuestro presente. Con el avance de la tecnología, internet y la creación de las redes sociales, "aquellos que escribimos el mundo pero no figuramos en la historia", somos esclavos del tiempo, hemos perdido la perspectiva del pasado y la máxima de que "el futuro es ahora" ya no tiene validez. El futuro es pasado y esto nos obliga a precipitarnos en una vida que no entendemos para ser aceptados socialmente. Ya no miramos ni analizamos el pasado para entender el presente y pensar el futuro. Por eso, repetimos los mismos esquemas que nos llevaron al fracaso, sin tiempo para reflexionar, dominados por infames horarios y manipulados por nuevas élites que disfrazan sus mensajes de "liberación" para ampliar el alcance de su dominio.

A nosotrxs, lxs hackers, lxs activistas, que también escribimos el mundo, no nos importa ser socialmente "aceptados" en el mundo de las élites. Luchamos contra su dominio, escuchando la resonancia de la muerte en la línea del tiempo para seguir alimentando conciencias, aunándo fuerzas para derrocar el sistema. Nosotrxs lxs hackers, lxs activistas, miramos al pasado orgullosxs de mantener viva la herencia de quienes nos precedieron en la lucha, aprendiendo de ellxs y de cómo usaron las herramientas de su época en contra del sistema de opresión.

Por eso, este año os convocamos al HACKSTAÑAZO, el hackmeeting de 2024 que se celebrará en Gaztaño, una isla donde la palabra tiempo aún mantiene su naturalidad y la resistencia es una herencia consciente. Una tierra donde aún está viva la memoria de las brujas, como Maritxu Mozkorra, que resistieron el desencantemiento forzado del mundo por las élites de la modernidad. En este encuentro os invitamos a reencantar la tecnología con el pensamiento y la rebeldía hacker, a compartir conocimientos y experiencias para construir nuevos medios de lucha o adaptar los que hemos heredado de otras épocas a los nuevos escenarios de confrontación social.

Nosotrxs, lxs hacker, lxs activistas, al igual que Prometeo, queremos seguir manteniendo el fuego de la lucha. Nosotrxs Resistimos!!! Viva la Resistencia!!

## Nota de prensa

**[Lee la nota de prensa aquí](/docs/press)**

{{< article link="/docs/about/nota-de-prensa" >}}

## Historia y pilares

{{< alert "edit">}}
**TODO!**
{{< /alert >}}
