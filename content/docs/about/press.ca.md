---
title: "Nota de premsa"
tags: ["Nota de premsa"]
weight: 1
slug: premnsa
---

Els pròxims dies del 26 al 29 de setembre se celebrarà el Hackmeeting d'enguany en Gaztaño (Orereta), Guipúscoa. El 
Hackmeeting és una trobada anual autogestionada que es porta celebrant des de l'any 2000 itinerant pel tot l'estat 
espanyol. Reuneix veïnes del lloc, artistes, activistes socials i activistes informàtiques (hacktivistes), vingudes de 
tot el territori estatal i dels veïns, especialment de terres italianes.

Conviuran 4 dies compartint coneixements, debatent, jugant, trastejant i fent xarxa, tant d'internet com social. La 
graella per als 4 dies organitza en 4 sales tots els "nodes", que són qualsevol activitat autoconvocada: xerrades, 
tallers, debats, jocs, ... i a la nit, una mica d'humor amb les xerrades ràpides i el karaoke propi, i una mica de festa 
amb concert i música punxada. Cada dia hi haurà una assemblea de gestió i la del diumenge es valorarà l'esdeveniment i 
es giraran responsabilitats en mires de l'any següent.

En aquests moments ja hi ha nodes apuntats sobre seguretat digital transfeminista, seguretat en telèfons, vigilància 
comercial i política, tecnologies decrecentistas i solarpunk, agendes comunitàries, sonorització d'esdeveniments, xarxes 
socials, intel·ligència artificial, xarxes de fibra òptica, diversitat i inclusivitat, debats sobre els reptes actuals 
de l'hacktivisme, i un llarg etcètera.

Enguany s'ha reservat un espai no mixt de gènere, on estan benvingudes totes les persones que no siguin homes cis-sexuals. 
Serà un espai amb nodes propis paral·lels als tres mixtos.

Hi ha publicada una graella provisional a data de 18 de setembre, que està subjecta a canvis i a més nodes. Aquesta es troba en http://lanbroa.maritxusarea.eus/s/*74Z3TAxNaHJrtzF/*download/*parrillafinala.png
Els canvis rellevants s'anunciaran en https://xarxa.cloud/@hackmeeting/

El manifest amb la reflexió i propòsit polític d'enguany es troba en https://es.hackmeeting.org/latest/hugo/docs/sobre/#manifest-2024

Per a més informació, consulteu https://es.hackmeeting.org/latest/hugo/ i el compte del fediverso https://xarxa.cloud/@hackmeeting/
